from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from .models import testimoniModels
from .forms import testimoni_form
from django.shortcuts import render, render_to_response, redirect
from django.views.decorators.csrf import csrf_exempt
import json

response= {}

def testimoni(request):
    testimoni = testimoniModels.objects.all()
    if(request.method == 'POST'):
        form = testimoni_form(request.POST or None)
        if(form.is_valid()):
            response['pesan_testimoni'] = request.POST['pesan_testimoni']
            testi = testimoniModels(pesan_testimoni=response['pesan_testimoni'])
            testi.save()
            return HttpResponseRedirect('/testimoni/')
    else:   
        form = testimoni_form()
    return render(request, 'testimoni.html', {'form':form, 'testimoni':testimoni})

@csrf_exempt
def create_post(request):
    if request.method == 'POST':
        post_text = request.POST.get('the_post')  
        pos = testimoniModels(pesan_testimoni = post_text)
        pos.save()
        
        response_data = {}
        try:
            response_data['result'] = "Your Message is post!"
            response_data['message'] = post_text
        except:
            response_data['result'] = "Your Message is not post yet!"
            response_data['message'] = "hayolo"

        return HttpResponse(json.dumps(response_data), content_type="application/json")