from django.urls import re_path
from .views import testimoni, create_post

app_name = 'testimoni'
urlpatterns = [
    re_path(r'^testimoni', testimoni ,name= "testimoni"),
    re_path(r'^create_post', create_post ,name= "create_post"),
    
    # re_path('<str:event_site>/', detail, name='details'),
]
