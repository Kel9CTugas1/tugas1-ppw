from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import testimoni, create_post
from .models import testimoniModels
from .forms import testimoni_form
import json

# Create your tests here.
class TestimoniUnitTest(TestCase):
    # Checking the url 
    def test_testimoni_url_is_exist(self):
        response = Client().get('/testimoni/')
        self.assertEqual(response.status_code, 200)

    # # Checking the url 
    # def test_createPost_url_is_exist(self):
    #     response = Client().get('/create_post/')
    #     self.assertEqual(response.status_code, 200)  
    
    # checking the function is available
    def test_testimoni_using_testimoni_func(self):
        found = resolve('/testimoni/')
        self.assertEqual(found.func, testimoni)

   # checking the function is available
    def test_createPost_using_testimoni_func(self):
        found = resolve('/create_post/')
        self.assertEqual(found.func, create_post)

    # Must fill the field to send feedback before send the post
    def test_validation_for_blank_status(self):
        form = testimoni_form(data={'pesan_testimoni': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['pesan_testimoni'], ["This field is required."])

    def test_post_success_and_render_the_result(self):
        test = 'TESTIMONI'
        response_post = Client().post('/testimoni/', {'pesan_testimoni': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/testimoni/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)	


    def test_can_save_a_POST_request(self):
        response = self.client.post('/testimoni/', data = {'pesan_testimoni' : 'Saya ingin tidur ya Allah'})
        counting_all_available_status = testimoniModels.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/testimoni/')

        new_response = self.client.get('/testimoni/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('', html_response)


    # test validation name
    # def test_post_valid(self):
    #     test = 'TESTIMONI'
    #     form_data = {'pesan_testimoni': 'Website ini keren'}        
    #     response_post = Client().post('/create_post/', form_data)
        # self.assertEqual(response_post.status_code, 302)

        # html_response = response_post.content.decode('utf8')
        # self.assertIn(test, html_response)


    # def test_post_success_and_render_the_result(self):
    #     test = 'TESTIMONI'
    #     response_post = Client().post('/testimoni/', {'pesan_testimoni': test})
    #     self.assertEqual(response_post.status_code, 302)

    #     response= Client().get('/testimoni/')
    #     html_response = response.content.decode('utf8')
    #     self.assertIn(test, html_response)	


    # # test validation name
    # def test_post_not_valid(self):
    #     response = self.client.post('/create_post/', data = {})
    #     counting_all_available_status = testimoniModels.objects.all().count()
    #     # response_post = Client().post('/create_post/', {'pesan_testimoni': })
    #     html_response = response.content.decode('utf8')
        
    #     self.assertIn(response, html_response)	
