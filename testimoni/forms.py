from django import forms
from django.forms import ModelForm
from .models import testimoniModels

class testimoni_form(forms.ModelForm):
	error_messages = {
		'required': 'Please fill this field',
	}
	
	testimoni_attrs = {
		'type': 'text',
		'id' : 'post-text',
		'cols': 160, 'rows': 8,
		'class': 'todo-form-textarea',
		'placeholder':'Type your feedback here!'
	}
	pesan_testimoni = forms.CharField(label = '', required = True, max_length = 300, widget = forms.Textarea(attrs = testimoni_attrs))

	class Meta:
		model = testimoniModels
		fields = ['pesan_testimoni']