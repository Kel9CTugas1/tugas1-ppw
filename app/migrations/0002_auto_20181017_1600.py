# Generated by Django 2.0.5 on 2018-10-17 09:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='berita',
            name='deskripsi',
            field=models.CharField(max_length=500),
        ),
    ]
