from django.db import models

# Create your models here.
class Berita(models.Model):
	link_img = models.URLField()
	judul = models.CharField(max_length=25)
	deskripsi = models.CharField(max_length=500)
	ke = models.IntegerField()