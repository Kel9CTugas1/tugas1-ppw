from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, dana

# Create your tests here.
class Lab6Test(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/app/')
		self.assertEqual(response.status_code, 200)

	def test_using_template(self):
		response = Client().get('/app/')
		self.assertTemplateUsed(response, 'index.html')

	def test_using_func(self):
		found = resolve('/app/')
		self.assertEqual(found.func, index)

	def test_galang_url_is_exist(self):
		response = Client().get('/dana/')
		self.assertEqual(response.status_code, 200)

	def test_galang_using_template(self):
		response = Client().get('/dana/')
		self.assertTemplateUsed(response, 'dana.html')

	def test_galang_using_func(self):
		found = resolve('/dana/')
		self.assertEqual(found.func, dana)

