from django.shortcuts import render
from django.http import HttpResponse

def index(request) :
    return render(request, 'index.html')

def dana(request):
	return render(request, 'dana.html')

def news(request):
	news = Berita.objects.all()
	response = {'news' : news}
	return render(request,'berita.html', response)

# def donasi(request):
#     return render(request, "Donasi.html", {})
