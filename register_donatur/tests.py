from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import donatur, add_donasi, daftar_donasi
from .forms import Donatur_Form
from .models import Donatur

# Create your tests here.
class donatur_test(TestCase):	

	def test_register_donatur_url_is_exist(self):
		response = Client().get('/register_donatur/')
		self.assertEqual(response.status_code, 200)
	def test_add_donasi_url_is_exist(self):
		response = Client().get('/add_donasi/')
		self.assertEqual(response.status_code, 200)
	def test_url_daftar_donasi(self):
		response = Client().get('/daftar_donasi/')
		self.assertEqual(response.status_code, 200)
	def test_template_donatur(self):
		response = Client().get('/register_donatur/')
		self.assertTemplateUsed(response, 'donatur.html')
	def test_regist_using_donatur_func(self):
		found = resolve('/register_donatur/')
		self.assertEqual(found.func, donatur)
	def test_add_donasi_using_add_donasi_func(self):
		found = resolve('/add_donasi/')
		self.assertEqual(found.func, add_donasi)
	def test_daftar_donasi_using_daftar_donasi_func(self):
		found = resolve('/daftar_donasi/')
		self.assertEqual(found.func, daftar_donasi)
	def test_model_can_add_new_don(self):
		don = Donatur.objects.create(nama_program= 'A', nama_lengkap='B', email= 'C@gmail.com',
            jumlah_donasi= 1, donasi_sebagai_anonim=True)
		counting_all_available_don = Donatur.objects.all().count()
		self.assertEqual(counting_all_available_don, 1)
	def test_form_validation_for_blank_items(self):
		form = Donatur_Form(data={'nama_program':'A',' nama_lengkap':'B', 'email':'C@gmail.com',
            'jumlah_donasi': 1, 'donasi_sebagai_anonim':True})
		self.assertFalse(form.is_valid())
		self.assertEqual(
            form.errors['nama_lengkap'],
            ["This field is required."]
        )
	def test_post(self):
		response = self.client.post('/register_donatur/') 
		data = {'nama_program':'A',
        		'nama_lengkap':'B', 
        		'email':'C@gmail.com',
            	'jumlah_donasi': 1, 
            	'donasi_sebagai_anonim':True}
		counting_all_available_status = Donatur.objects.all().count()
		self.assertEqual(counting_all_available_status, 0)
		self.assertEqual(response.status_code, 200)
        # self.assertEqual(response['location'], '/registrasi_app/regis/')

		new_response = self.client.get('/register_donatur/')
		html_response = new_response.content.decode('utf8')
		self.assertIn('', html_response)

	def test_form_todo_input_has_placeholder_and_css_classes(self):
		form = Donatur_Form()
		self.assertIn('class="form-control', form.as_p())

	def test_donatur_post_success_and_render_the_result(self):
		test = 'Anonymous'
		jumlah_donasi = 10
		donasi_sebagai_anonim = True
		response_post = Client().post('/add_donasi/', {'nama_program':test,' nama_lengkap':test, 'email':test,
            'jumlah_donasi': jumlah_donasi, 'donasi_sebagai_anonim':donasi_sebagai_anonim})
		self.assertEqual(response_post.status_code, 200)

	def test_donatur_post_error_and_render_the_result(self):	
		test = 'Anonymous'
		response_post = Client().post('/add_donasi/', {'nama_program':'',' nama_lengkap':'', 'email':'selvyfiriani@gmail.com',
            'jumlah_donasi': 0, 'donasi_sebagai_anonim':False})
		self.assertEqual(response_post.status_code, 200)

	def test_daftar_donasi_using_daftar_donasi_func(self):
		found = resolve('/daftar_donasi/')
		self.assertEqual(found.func, daftar_donasi)    

	def test_template_belum_login(self):
		response = Client().get('/daftar_donasi/')
		self.assertTemplateUsed(response, 'belum_login.html')
