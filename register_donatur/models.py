from django.db import models

# Create your models here.

class Donatur(models.Model):
	nama_program = models.CharField(max_length=27)
	nama_lengkap = models.CharField(max_length=27, default="Selvy Fitriani")
	email = models.EmailField(unique = False, default="selvyfiriani@gmail.com")
	jumlah_donasi = models.IntegerField()
	donasi_sebagai_anonim = models.BooleanField(default=False)

	# def __str__(self):
	# 	return self.nama_program