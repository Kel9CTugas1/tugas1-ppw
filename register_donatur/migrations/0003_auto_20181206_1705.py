# Generated by Django 2.1.1 on 2018-12-06 10:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('register_donatur', '0002_auto_20181020_2054'),
    ]

    operations = [
        migrations.AlterField(
            model_name='donatur',
            name='email',
            field=models.EmailField(max_length=254),
        ),
    ]
