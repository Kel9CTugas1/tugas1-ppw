from django import forms
from .models import Donatur
from donasi.views import donasiModels

class Donatur_Form(forms.Form):
    error_messages = {
        'required': 'This field is required.',
    }
    program_attrs = {
        'type': 'text',
        'class': 'donatur-form-input form-control form-style',
        'placeholder':'Nama program',
        'id': 'nama_program'
    }
    nama_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Nama lengkap',
        'id': 'nama_lengkap'
    }
    email_attrs = {
        'type':'email',
        'class': 'form-control form-style',
        'placeholder':'Email',
        'id': 'email'
    }
    donasi_attrs ={
        'type' : 'number',
        'step' : 100000,
        'class': 'form-control form-style',
        'placeholder': 'Jumlah donasi',
        'id': 'jumlah_donasi'
    }

    anonim_attrs ={
        'id':'donasi_sebagai_anonim'
    }

    OPTION = [(x.name, x.name) for x in donasiModels.objects.all()]

    nama_program = forms.ChoiceField(label='', choices=OPTION)
    nama_lengkap = forms.CharField(label='', required=True, max_length=27, widget=forms.widgets.TextInput(attrs=nama_attrs))
    email = forms.EmailField(label ='', required=True, widget=forms.TextInput(attrs=email_attrs))
    jumlah_donasi = forms.IntegerField(label='', required=True, min_value=100000, widget=forms.NumberInput(attrs=donasi_attrs))
    donasi_sebagai_anonim = forms.BooleanField(label="Donasi sebagai anonim", widget=forms.CheckboxInput(attrs=anonim_attrs), required=False) 

    def clean(self):
        cleaned_data = super().clean()
        nama_lengkap = cleaned_data.get("nama_lengkap")
        donasi_sebagai_anonim = cleaned_data.get("donasi_sebagai_anonim")
        if not donasi_sebagai_anonim and not nama_lengkap:
            raise forms.ValidationError(
                "Nama kosong"
            )
            
# class Meta:
#     model = Donatur
#     fields = ['nama_program', 'nama_lengkap', 'email', 'jumlah_donasi', 'donasi_sebagai_anonim']
