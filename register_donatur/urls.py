from django.conf.urls import url
from django.urls import path
from register_donatur import views
from .views import donatur, add_donasi, daftar_donasi, donasi_list_json

urlpatterns = [
	url('register_donatur', donatur, name='donatur'),
	url('add_donasi/', add_donasi, name='add_donasi'),
	url('daftar_donasi/', daftar_donasi, name='daftar_donasi'),
	url('donasi_list_json/', donasi_list_json, name='donasi_list_json'),
]