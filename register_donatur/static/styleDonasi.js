 // Submit post on submit
 $('#form').on('submit', function(event){
    event.preventDefault();
    console.log("form submitted!")  // sanity check
    create_post();
});

// AJAX for posting
function create_post() {
    console.log("create post is working!") // sanity check
    console.log($('#post-text').val())

    $.ajax({
        type : "POST", 
        url : "/create_post/",                                      //add the url for create post in views
        async: true,
        data : { 
            csrfmiddlewaretoken: '{{ csrf_token }}',                //to handle the csrf
            the_post : $('#post-text').val() },                     //send the data
        
        //When not success
        success : function(json) {
            $('#post-text').val('');                                //empty the field
            $('#ea').prepend("<article class='row'>"
                                +"<div class='col-md-2 col-sm-2 hidden-xs'>"
                                + "<figure class='thumbnail'>"
                                +"<img class='img-responsive' src='https://s.tcdn.co/e78/90b/e7890b10-1c4c-3cd4-a176-4e1010807ace/192/1.png' />"
                                + "<figcaption class='text-center'>username</figcaption></figure></div>"
                                +"<div class='col-md-10 col-sm-10'>"
                                +"<div class='panel panel-default arrow left'>"
                                +"<div class='panel-body'>"
                                +"<header class='text-left'>"
                                +"<div class='comment-user'><i class='fa fa-user'></i> Username </div></header>"
                                +"<div class='comment-post'>"
                                +"<p>" + json.message +"</p></div>"
                                +"<p style='color:red'>*Your message will be save. And this is your preview message</p>"                                
                                );
        alert("Your message will be save. And this is your preview message ");
        
        },

        // When not success
        error : function(xhr,errmsg,err) {
            $('#post-text').val('');                                // empty the field
            console.log("fail");                                    // Said fail
            // console.log(xhr.status); // provide a bit more info about the error to the console
        }
    });

};

