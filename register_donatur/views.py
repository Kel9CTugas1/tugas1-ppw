from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from .models import Donatur
from .forms import Donatur_Form
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Sum
import json

response = {'author': "Selvy Fitriani"}

# Create your views here.
def donatur(request) :
    donatur = Donatur.objects.all()
    response['donatur'] = donatur
    response['Donatur_Form'] = Donatur_Form
    return render(request, 'donatur.html', response)

@csrf_exempt
def add_donasi(request) :
    form = Donatur_Form(request.POST or None)
    if(request.method == 'POST'):
        # if 'nama_program' in request.POST:
        nama_program = request.POST["nama_program"]
        # else:
        #     nama_program = "Program tidak terdefinisi"
        if request.user.is_authenticated:
            nama_lengkap = request.user.first_name
            email = request.user.email
        else :
            nama_lengkap = 'Anonim'
            email = 'Not found'
        jumlah_donasi = request.POST["jumlah_donasi"]
        status = True
        if request.POST.get("donasi_sebagai_anonim"):
            donatur = Donatur(nama_program= nama_program, nama_lengkap=nama_lengkap, email= email,
            jumlah_donasi= jumlah_donasi, donasi_sebagai_anonim=True)
        else:
            donatur = Donatur(nama_program= nama_program, nama_lengkap=nama_lengkap, email= email,
            jumlah_donasi=jumlah_donasi, donasi_sebagai_anonim=False)
        donatur.save()
        # return HttpResponse(json.dumps({'is_success':status, 'is_yey':'yay'}), content_type = 'application/json') 
        return JsonResponse({'is_success':status, 'is_yey':'yay'}) 
        # return render(request, 'donatur.html', response)
    else:
        status = False
        # return HttpResponse(json.dumps({'is_success':status, 'is_yey':'nay'}), content_type = 'application/json')
        return JsonResponse({'is_success':status, 'is_yey':'nay'}) 
        # HttpResponseRedirect('/register_donatur/')
    # return JsonResponse({'is_success':status, 'is_yey':'yay'})

def donasi_list_json(request) :
    donasi = [obj.as_dict() for obj in Donatur.objects.all().filter(email = request.user.email)]
    return JsonResponse({"results" : donasi}, content_type = 'application/json')

def daftar_donasi(request):
    if request.user.is_authenticated:
        if 'nama' not in request.session:
            request.session['nama'] = request.user.first_name + " " + request.user.last_name
        if 'username' not in request.session:
            request.session['username'] = request.user.username
        if 'email' not in request.session:
            request.session['email'] = request.user.email
        if request.user.is_active:
            donatur = Donatur.objects.all().filter(email = request.user.email)
            response['donatur'] = donatur
            response['Donatur_Form'] = Donatur_Form
            response['total'] = Donatur.objects.all().filter(email = request.user.email).aggregate(Sum('jumlah_donasi'))['jumlah_donasi__sum']
            return render(request, 'daftar_donasi.html', response)
        else:
            return render(request, 'belum_login.html', {})
    else:
        return render(request, 'belum_login.html', {})


