from django.conf.urls import url
from .views import berita,detail

urlpatterns = [
    url('berita', berita),
    url('detail', detail),
    url('', berita),

]