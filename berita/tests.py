from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import berita, detail

# Create your tests here.
class BeritaUnitTest(TestCase):
    def test_news_url_is_exist(self):
        response = Client().get('/berita/')
        self.assertEqual(response.status_code, 200)

    def test_news_using_berita_func(self):
        found = resolve('/berita/')
        self.assertEqual(found.func, berita)

    # def test_news_render_the_result(self):
    #     test = 'Berita'
    #     response= Client().get('/detail/')
    #     html_response = response.content.decode('utf8')
    #     self.assertIn(test, html_response)
