from django.apps import AppConfig


class RegistrasiAppConfig(AppConfig):
    name = 'registrasi_app'
