from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Regist
from .forms import Regist_Form

response = {'author': "Cassie Michelle"}
# Create your views here.
def registrasi(request) :
    regist = Regist.objects.all()
    response['regist'] = regist
    response['regist_form'] = Regist_Form
    return render(request, 'registrasi.html', response)

def add_user(request) :
    form = Regist_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['nama'] = request.POST['nama']
        response['tmpt_lahir'] = request.POST['tmpt_lahir']
        response['tgl_lahir'] = request.POST['tgl_lahir']
        response['email'] = request.POST['email']
        response['password'] = request.POST['password']
        regist = Regist(nama=response['nama'],tmpt_lahir=response['tmpt_lahir'],tgl_lahir=response['tgl_lahir'],
        email=response['email'], password=response['password'])
        mail = Regist.objects.all().filter(email = response['email']).count()

        if mail == 0:
            regist.save()
            regist = Regist.objects.all()
            response['regist'] = regist
            response['terdaftar'] = 'user berhasil ditambahkan'
            return render(request, 'registrasi.html', response)
        else:
            response['terdaftar'] = 'email sudah terdaftar'
            return render(request, 'registrasi.html', response)
    else:
        return HttpResponseRedirect('/registrasi_app/')
