from django.db import models

# Create your models here.

class Regist(models.Model):
    nama = models.CharField(max_length=27)
    tmpt_lahir = models.CharField(max_length=27)
    tgl_lahir = models.DateField()
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=27)
    # description = models.TextField()
    # created_date = models.DateTimeField(auto_now_add=True)