from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import registrasi, add_user
from .models import Regist
from .forms import Regist_Form

# Create your tests here.
class RegistUnitTest(TestCase):
    def test_regist_url_is_exist(self):
        response = Client().get('/registrasi_app/regis')
        self.assertEqual(response.status_code, 200)

    def test_logoutgoogle_url_is_exist(self):
        response = Client().get('/registrasi_app/regis/logout')
        self.assertEqual(response.status_code, 200)
    
    def test_template_registrasi(self):
        response = Client().get('/registrasi_app/regis')
        self.assertTemplateUsed(response, 'registrasi.html')
        
    def test_regist_using_index_func(self):
        found = resolve('/registrasi_app/regis')
        self.assertEqual(found.func, registrasi)

    def test_regist_using_add_user_func(self):
        found = resolve('/registrasi_app/add_user')
        self.assertEqual(found.func, add_user)

    def test_model_can_add_new_reg(self):
        reg = Regist.objects.create(nama='Cassie', tgl_lahir='1999-08-16', tmpt_lahir='Bandung', email='michellecassie@gmail.com', password='1234')
        counting_all_available_reg = Regist.objects.all().count()
        self.assertEqual(counting_all_available_reg, 1)

    def test_form_validation_for_blank_items(self):
        form = Regist_Form(data={'nama': '', 'tmpt_lahir': '', 'tgl_lahir':'', 'email':'', 'password':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['nama'],
            ["This field is required."]
        )

    def test_post(self):
        response = self.client.post('/registrasi_app/regis/', 
        data = {'nama' : 'Saya ingin tidur ya Allah',
                'tmpt_lahir': '', 
                'tgl_lahir':'', 
                'email':'', 
                'password':''})
        counting_all_available_status = Regist.objects.all().count()
        self.assertEqual(counting_all_available_status, 0)

        self.assertEqual(response.status_code, 200)
        # self.assertEqual(response['location'], '/registrasi_app/regis/')

        new_response = self.client.get('/registrasi_app/regis/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('', html_response)


    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = Regist_Form()
        self.assertIn('class="regist-form-input', form.as_p())

    def test_regist_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/registrasi_app/add_user', {'nama': test, 'tmpt_lahir': test, 'tgl_lahir':'1999-08-16', 'email': test, 'password': test})
        self.assertEqual(response_post.status_code, 302)

    def test_regist_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/registrasi_app/add_user', {'nama': '', 'tmpt_lahir':'', 'tgl_lahir':'1999-08-16', 'email': '', 'password': ''})
        self.assertEqual(response_post.status_code, 302)

    def test_model_check_items_in_model_double_email(self):
        test = 'anonymous'
        coba = '2018-01-01'
        email = 'coba@hai.com'
        Client().post('/registrasi_app/add_user', {'nama': test , 'tmpt_lahir': test , 'tgl_lahir':'1999-08-16', 'email': email , 'password': test})
        counting_all_available_todo = Regist.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)
        Client().post('/registrasi_app/add_user', {'nama': test, 'tmpt_lahir': test, 'tgl_lahir':'1999-08-16', 'email': email, 'password': test})
        counting_all_available_todo = Regist.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)



    

