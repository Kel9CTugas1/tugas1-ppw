from django.conf.urls import url
from django.urls import path, re_path, include
from django.contrib.auth import views
from .views import registrasi, add_user
from django.conf import settings

urlpatterns = [
    url('add_user', add_user , name='add_user'),
    url('regis', registrasi),
    path('login', views.LoginView.as_view(), name="login"),
    path('logout', views.LogoutView.as_view(), {'next_page' : settings.LOGOUT_REDIRECT_URL}, name="login"),
        re_path('auth/', include('social_django.urls', namespace='social')),  # <- Here
]