
from django import forms
from .models import Regist

class Regist_Form(forms.Form):
    error_messages = {
        'required': 'This field is required.',
    }
    nama_attrs = {
        'type': 'text',
        'class': 'regist-form-input form-control form-style',
        'placeholder':'Input your name'
    }
    tmpt_attrs = {
        'type': 'text',
        'class': 'form-control form-style',
        'placeholder':'Input your birth place'
    }
    tgl_attrs = {
        'type':'date',
        'class': 'form-control form-style',
        'placeholder':'Input your birthday'
    }
    email_attrs = {
        'type': 'email',
        'class': 'form-control form-style',
        'placeholder': 'Input your email address'
    }
    password_attrs ={
        'type': 'password',
        'class': 'form-control form-style',
        'placeholder': 'Password'
    }

    nama = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=nama_attrs))
    tmpt_lahir = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=tmpt_attrs))
    tgl_lahir = forms.DateField(label ='', required=True , widget=forms.DateInput(attrs=tgl_attrs))
    email = forms.EmailField(label='',required = True, widget=forms.EmailInput(attrs=email_attrs))
    password = forms.CharField(label='',required = True, widget=forms.TextInput(attrs=password_attrs) )

