from django.test import TestCase, Client
from django.urls import resolve
from .views import donasi

class statusWebTest(TestCase):
	def test_url_donasi(self):
		response = Client().get('/donasi/')
		self.assertEqual(response.status_code, 200)

	def test_template_donasi(self):
		response = Client().get('/donasi/')
		self.assertTemplateUsed(response, 'Donasi.html')

	def test_func_donasi(self):
		found = resolve('/donasi/')
		self.assertEqual(found.func, donasi)