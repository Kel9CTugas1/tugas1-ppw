import datetime
from django.db import models
from django.utils import timezone

class donasiModels(models.Model):
    name = models.CharField(max_length=100)
    lokasi = models.CharField(max_length=100)
    image_url = models.CharField(max_length=300)

    class Meta:
        verbose_name_plural = "donasi"

    # def __str__(self):
    #     return self.name
