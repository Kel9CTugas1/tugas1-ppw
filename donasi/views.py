# from django.shortcuts import render
# from django.http import HttpResponse

# def donasi(request):
#     return render(request, "Donasi.html", {})

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from .models import donasiModels

def donasi(request):
	all_events = donasiModels.objects.all()
	return render(request, 'Donasi.html', {'all_events': all_events})

# def detail(request, event_site):
#     event = get_object_or_404(Event, site=event_site)
#     return render(request, 'detail.html', {'event': event})
