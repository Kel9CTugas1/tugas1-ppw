# Tugas 1 PPW Kelompok 9

## Anggota Kelompok
1. Falya Aqiela Sekardina 1706027673
2. Cassie Michelle 1706043506
3. Mila Shania 1706026821
4. Selvy Fitriani 1706039446

## Status Aplikasi
[![Pipeline](https://gitlab.com/Kel9CTugas1/tugas1-ppw/badges/master/pipeline.svg)](https://gitlab.com/Kel9CTugas1/tugas1-ppw/commits/master)
[![Coverage](https://gitlab.com/Kel9CTugas1/tugas1-ppw/badges/master/coverage.svg)](https://gitlab.com/Kel9CTugas1/tugas1-ppw/commits/master)

## Link Heroku App
http://crowdfund9c.herokuapp.com/
